/* 
 * File:   DrawFractalArguments.hpp
 * Author: Revers
 *
 * Created on 3 maj 2012, 09:32
 */

#ifndef DRAWFRACTALARGUMENTS_HPP
#define	DRAWFRACTALARGUMENTS_HPP

namespace DrawMandelbrotFractal {
    enum Arguments {
        ARG_OUTPUT = 0,
        ARG_IMAGE_W,
        ARG_IMAGE_H,
        ARG_MIN_RE,
        ARG_MAX_IM,
        ARG_ONE_PIXEL_SIZE,
        ARG_AA_LEVEL,
        ARG_AA_LEVEL_INV,
        ARG_AA_SCALE,
        ARG_ITERATIONS,
        ARG_ESCAPE_RADIUS_SQR,
        ARG_CONST_POINT
    };
}

namespace DrawJuliaFractal {
    enum Arguments {
        ARG_OUTPUT = 0,
        ARG_IMAGE_W,
        ARG_IMAGE_H,
        ARG_MIN_RE,
        ARG_MAX_IM,
        ARG_ONE_PIXEL_SIZE,
        ARG_AA_LEVEL,
        ARG_AA_LEVEL_INV,
        ARG_AA_SCALE,
        ARG_ITERATIONS,
        ARG_ESCAPE_RADIUS_SQR,
        ARG_CONST_POINT
    };
}

#endif	/* DRAWFRACTALARGUMENTS_HPP */

