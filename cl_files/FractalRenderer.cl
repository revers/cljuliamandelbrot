#ifdef FAKE_INCLUDE
#include "../../FAKE_INCLUDE/opencl_fake.hxx"
#endif

#include "Complex.cl"

#ifndef float_type 
#ifdef USE_DOUBLE
cl_khr_fp64
cl_amd_fp64
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#define float_type double
#define float_type2 double2
#else
#define float_type float
#define float_type2 float2
#endif
#endif

#ifndef DIVIDE
#define DIVIDE(a, b) native_divide(a, b)
#endif

#ifndef SQRT
#define SQRT(x) native_sqrt(x)
#endif

#ifdef USE_DOUBLE
#define COSINE(x) cos(x)
#define LOG2(x) log2(x)
#else
#define COSINE(x) native_cos(x)
#define LOG2(x) native_log2(x)
#endif


#define float_t(f) ((float_type)f)

float4 getMandelbrotColor(Complex point,
        unsigned int iterations,
        float_type escapeRadiusSqr) {
    Complex z = point;

    unsigned n;
    float_type zSquare;

    for (n = 0; n < iterations; ++n) {
        z = cMul(z, z) + point;


        zSquare = dot(z, z);
        if (zSquare > escapeRadiusSqr) {
            break;
        }
    }

    if (n < iterations) {
        float_type R = 0.0;
        float_type G = 0.4;
        float_type B = 0.7;

        float_type co = float_t(n) + float_t(1.0)
                - LOG2(float_t(0.5) * LOG2(zSquare));
        co = SQRT(co * float_t(0.00390625));

        float4 col = (float4) (
                float_t(0.5) + float_t(0.5) * COSINE(float_t(6.2831) * co + R),
                float_t(0.5) + float_t(0.5) * COSINE(float_t(6.2831) * co + G),
                float_t(0.5) + float_t(0.5) * COSINE(float_t(6.2831) * co + B),
                1.0f);

        return col;
    } else {
        return (float4) (0, 0, 0, 1);
    }
}

__kernel void drawMandelbrotFractal(
        __write_only image2d_t output,
        uint imageW,
        uint imageH,
        float_type minRe,
        float_type maxIm,
        Complex onePixelSize,
        int aaLevel,
        float_type aaLevelInv,
        Complex aaScale,
        unsigned int iterations,
        float_type escapeRadiusSqr,
        Complex constPoint) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= imageW || y >= imageH) {
        return;
    }

    Complex currentPoint = (Complex) (minRe + x * onePixelSize.x,
            maxIm - y * onePixelSize.y);
    
    float4 color = (float4) (0.0, 0.0, 0.0, 0.0);

    Complex temp = constPoint - currentPoint;
    if (dot(temp, temp) < float_t(10.0) * onePixelSize.x * onePixelSize.x) {
        color = (float4) (1, 0, 0, 1);
    } else {

        for (int x = 0; x < aaLevel; x++) {
            for (int y = 0; y < aaLevel; y++) {
                color += getMandelbrotColor(currentPoint
                        + (Complex) (x, y) * aaLevelInv * aaScale,
                        iterations, escapeRadiusSqr);
            }
        }

        color *= aaLevelInv * aaLevelInv;
        //  color = getColor2D(currentPoint,
        //  iterations, escapeRadiusSqr, pallet);
        color.w = 1.0;
    }

    write_imagef(output, (int2) (x, y), color);
}

//==============================================================================

float4 getJuliaColor(Complex point,
        unsigned int iterations,
        float_type escapeRadiusSqr,
        Complex constPoint) {

    Complex z = point;

    unsigned n;
    float_type zSquare;

    for (n = 0; n < iterations; ++n) {
        z = cMul(z, z) + constPoint;


        zSquare = dot(z, z);
        if (zSquare > escapeRadiusSqr) {
            break;
        }
    }

    if (n < iterations) {
        float_type R = 0.0;
        float_type G = 0.4;
        float_type B = 0.7;

        float_type co = float_t(n) + float_t(1.0)
                - LOG2(float_t(0.5) * LOG2(zSquare));
        co = SQRT(co * float_t(0.00390625)); // co / 256.f

        float4 col = (float4) (
                float_t(0.5) + float_t(0.5) * COSINE(float_t(6.2831) * co + R),
                float_t(0.5) + float_t(0.5) * COSINE(float_t(6.2831) * co + G),
                float_t(0.5) + float_t(0.5) * COSINE(float_t(6.2831) * co + B),
                1.0f);

        return col;
    } else {
        return (float4) (0, 0, 0, 1);
    }
}

__kernel void drawJuliaFractal(
        __write_only image2d_t output,
        uint imageW,
        uint imageH,
        float_type minRe,
        float_type maxIm,
        Complex onePixelSize,
        int aaLevel,
        float_type aaLevelInv,
        Complex aaScale,
        unsigned int iterations,
        float_type escapeRadiusSqr,
        Complex constPoint) {

    uint x = get_global_id(0);
    uint y = get_global_id(1);

    if (x >= imageW || y >= imageH) {
        return;
    }

    Complex currentPoint = (Complex) (minRe + x * onePixelSize.x,
            maxIm - y * onePixelSize.y);

    float4 color = (float4) (0.0, 0.0, 0.0, 0.0);

    for (int x = 0; x < aaLevel; x++) {
        for (int y = 0; y < aaLevel; y++) {
            color += getJuliaColor(currentPoint
                    + (Complex) (x, y) * aaLevelInv * aaScale,
                    iterations, escapeRadiusSqr, constPoint);
        }
    }

    color *= aaLevelInv * aaLevelInv;
    //  color = getColor2D(currentPoint,
    //  iterations, escapeRadiusSqr, pallet);
    color.w = 1.0;

    write_imagef(output, (int2) (x, y), color);
}