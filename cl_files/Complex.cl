#ifndef COMPLEX_CL
#define COMPLEX_CL

#ifdef FAKE_INCLUDE
#include "../../FAKE_INCLUDE/opencl_fake.hxx"
#endif

#ifndef float_type 
#ifdef USE_DOUBLE
#pragma OPENCL EXTENSION cl_khr_fp64 : enable
#pragma OPENCL EXTENSION cl_amd_fp64 : enable
#define float_type double
#define float_type2 double2
#else
#define float_type float
#define float_type2 float2
#endif
#endif

typedef float_type2 Complex;

__attribute__((always_inline))
inline Complex cAdd(Complex a, Complex b) {
    return a + b;
}

__attribute__((always_inline))
inline Complex cSubtract(Complex a, Complex b) {
    return a - b;
}

__attribute__((always_inline))
inline Complex cMul(Complex a, Complex b) {
	return (Complex)( a.x*b.x -  a.y*b.y,a.x*b.y + a.y * b.x);
}

__attribute__((always_inline))
inline Complex cDiv(Complex a, Complex b) {
    float_type div = (b.x * b.x) + (b.y * b.y);
    Complex tmp = (Complex)(a.x * b.x + a.y * b.y,
            a.y * b.x - a.x * b.y);
    return tmp / div;
}

__attribute__((always_inline))
inline Complex cPower(Complex z, float_type n) {
	float_type r2 = dot(z,z);
	return pow(r2, n * (float_type) 0.5) 
                * (Complex)(cos(n*atan(z.y/z.x)), sin(n*atan(z.y/z.x)));
}

__attribute__((always_inline))
inline Complex cInverse(Complex a) {
       return	(Complex)(a.x,-a.y)/dot(a,a);
}

__attribute__((always_inline))
inline Complex cExp(Complex z) {
	return (Complex)(exp(z.x) * cos(z.y), exp(z.x) * sin(z.y));
}

__attribute__((always_inline))
inline Complex cLog(Complex a) {
	float_type b =  atan2(a.y,a.x);
	if (b>0.0) b-=2.0*3.1415;
	return (Complex)(log(length(a)),b);
}

__attribute__((always_inline))
inline Complex cSin(Complex z) {
  return (Complex)(sin(z.x)*cosh(z.y), cos(z.x)*sinh(z.y));
}

__attribute__((always_inline))
inline Complex cCos(Complex z) {
  return (Complex)(cos(z.x)*cosh(z.y), -sin(z.x)*sinh(z.y));
}

#endif