/* 
 * File:   ComplexType.hpp
 * Author: Revers
 *
 * Created on 5 maj 2012, 10:36
 */

#ifndef COMPLEXTYPE_HPP
#define	COMPLEXTYPE_HPP

#include "Configuration.h"

struct Complex {

    union {

        struct {
            float_type x;
            float_type y;
        };

        struct {
            float_type re;
            float_type im;
        };
    };
    
    Complex() {}
    Complex(float_type re_, float_type im_) : re(re_), im(im_) {}
};


#endif	/* COMPLEXTYPE_HPP */

