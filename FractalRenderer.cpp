/* 
 * File:   FractalRenderer.cpp
 * Author: Revers
 * 
 * Created on 17 marzec 2012, 16:39
 */

#include <GL/glew.h>
#include <CL/cl_gl.h>
#include <GL/gl.h>

#include "FractalRenderer.h"
#include <iostream>

#include <RevUtil.hpp>

using namespace std;

#define KERNEL_FILE "cl_files/FractalRenderer.cl"
#define FRACTAL_SHADER_FILE "shaders/FractalRenderer.glsl"


#define CHECK_ERR(A) if(!(A)) { assert((A)); return false; }

#define LOCAL_SIZE_X 16
#define LOCAL_SIZE_Y 16

bool FractalRenderer::init() {
    if (!initCL()) {
        cout << "initCL() FAILED!!" << endl;
        return false;
    }

    if (!createProgramAndKernels()) {
        cout << "createProgramAndKernels() FAILED!!" << endl;
        return false;
    }

    if (width != 0 && height != 0) {
        inited = true;
        if (!initImageBuffer()) {
            cout << "ERROR: initImageBuffer() FAILED!!" << endl;
            inited = false;
            return false;
        } else {
            cout << "initImageBuffer() SUCCESS" << endl;
        }
    }

    if (!fractalProgram.compileShaderGLSL(FRACTAL_SHADER_FILE)) {
        cout << FRACTAL_SHADER_FILE << " compilation FAILED!" << endl;
        return false;
    } else if (!fractalProgram.link()) {
        cout << FRACTAL_SHADER_FILE << " linking FAILED!" << endl;
        return false;
    }


#ifdef JULIA_ON_RIGHT_SIDE
    float mandelbrotXBegin = -1.0f;
    float mandelbrotXEnd = 0.0f;
    float juliaXBegin = 0.0f;
    float juliaYEnd = 1.0f;
#else
    float mandelbrotXBegin = 0.0f;
    float mandelbrotXEnd = 1.0f;
    float juliaXBegin = -1.0f;
    float juliaYEnd = 0.0f;
#endif

    quadVAOMandelbrot = createQuadVAO(mandelbrotXBegin, mandelbrotXEnd);
    if (quadVAOMandelbrot == 0) {
        cout << "fsQuadMandelbrot -> createQuad() FAILED!" << endl;
        return false;
    }

    quadVAOJulia = createQuadVAO(juliaXBegin, juliaYEnd);
    if (quadVAOJulia == 0) {
        cout << "fsQuadJulia -> createQuad() FAILED!" << endl;
        return false;
    }


    fractalProgram.use();
    fractalProgram.setUniform("Tex1", 0);
    glUseProgram(0);
    
    setJuliaConstPoint(juliaConstPoint);

    inited = true;

    return true;
}

void FractalRenderer::drawFractal() {
    glFlush();
    glFinish();

    const size_t localSize[] = {LOCAL_SIZE_X, LOCAL_SIZE_Y};

    cl_int err = clCommandQueue.enqueueNDRangeKernel(
            kernelDrawMandelbrotFractal, cl::NullRange, cl::NDRange(gridSize[0], gridSize[1]),
            cl::NDRange(localSize[0], localSize[1]), NULL, NULL);
    assert(err == CL_SUCCESS);
    
    clCommandQueue.finish();

    err = clCommandQueue.enqueueNDRangeKernel(
            kernelDrawJuliaFractal, cl::NullRange, cl::NDRange(gridSize[0], gridSize[1]),
            cl::NDRange(localSize[0], localSize[1]), NULL, NULL);
    assert(err == CL_SUCCESS);

    clCommandQueue.flush();
    clCommandQueue.finish();
}

void FractalRenderer::render() {

    drawFractal();

    fractalProgram.use();

    glActiveTexture(GL_TEXTURE0);
    glBindTexture(GL_TEXTURE_2D, textureMandelbrot);

    glBindVertexArray(quadVAOMandelbrot);
    glDrawArrays(GL_TRIANGLES, 0, 6);

    glBindTexture(GL_TEXTURE_2D, textureJulia);

    glBindVertexArray(quadVAOJulia);
    glDrawArrays(GL_TRIANGLES, 0, 6);
}

void FractalRenderer::zoomInMandelbrot() {

    float_type newDistRe = boundariesMandelbrot.getDistRe() / zoomFactor;
    float_type newDistIm = boundariesMandelbrot.getDistIm() / zoomFactor;
    zoomMandelbrot /= zoomFactor;

    float_type offsetRe = (boundariesMandelbrot.getDistRe() - newDistRe) / 2.0;
    float_type offsetIm = (boundariesMandelbrot.getDistIm() - newDistIm) / 2.0;
    boundariesMandelbrot.setMinRe(boundariesMandelbrot.getMinRe() + offsetRe);
    boundariesMandelbrot.setMinIm(boundariesMandelbrot.getMinIm() + offsetIm);

    boundariesMandelbrot.setMaxRe(boundariesMandelbrot.getMaxRe() - offsetRe);
    boundariesMandelbrot.setMaxIm(boundariesMandelbrot.getMaxIm() - offsetIm);

    setBoundaryArgumentsMandelbrot();
}

void FractalRenderer::zoomOutMandelbrot() {

    float_type newDistRe = boundariesMandelbrot.getDistRe() * zoomFactor;
    float_type newDistIm = boundariesMandelbrot.getDistIm() * zoomFactor;
    zoomMandelbrot *= zoomFactor;

    float_type offsetRe = (newDistRe - boundariesMandelbrot.getDistRe()) / 2.0;
    float_type offsetIm = (newDistIm - boundariesMandelbrot.getDistIm()) / 2.0;
    boundariesMandelbrot.setMinRe(boundariesMandelbrot.getMinRe() - offsetRe);
    boundariesMandelbrot.setMinIm(boundariesMandelbrot.getMinIm() - offsetIm);

    boundariesMandelbrot.setMaxRe(boundariesMandelbrot.getMaxRe() + offsetRe);
    boundariesMandelbrot.setMaxIm(boundariesMandelbrot.getMaxIm() + offsetIm);

    setBoundaryArgumentsMandelbrot();
}

void FractalRenderer::moveMandelbrot(int shiftX, int shiftY) {

    float_type shiftRe = onePixelSizeMandelbrot.re * shiftX;
    float_type shiftIm = onePixelSizeMandelbrot.im * shiftY;

    boundariesMandelbrot.setMinRe(boundariesMandelbrot.getMinRe() + shiftRe);
    boundariesMandelbrot.setMinIm(boundariesMandelbrot.getMinIm() + shiftIm);

    boundariesMandelbrot.setMaxRe(boundariesMandelbrot.getMaxRe() + shiftRe);
    boundariesMandelbrot.setMaxIm(boundariesMandelbrot.getMaxIm() + shiftIm);

    setBoundaryArgumentsMandelbrot();
}

void FractalRenderer::zoomInJulia() {

    float_type newDistRe = boundariesJulia.getDistRe() / zoomFactor;
    float_type newDistIm = boundariesJulia.getDistIm() / zoomFactor;
    zoomJulia /= zoomFactor;

    float_type offsetRe = (boundariesJulia.getDistRe() - newDistRe) / 2.0;
    float_type offsetIm = (boundariesJulia.getDistIm() - newDistIm) / 2.0;
    boundariesJulia.setMinRe(boundariesJulia.getMinRe() + offsetRe);
    boundariesJulia.setMinIm(boundariesJulia.getMinIm() + offsetIm);

    boundariesJulia.setMaxRe(boundariesJulia.getMaxRe() - offsetRe);
    boundariesJulia.setMaxIm(boundariesJulia.getMaxIm() - offsetIm);

    setBoundaryArgumentsJulia();
}

void FractalRenderer::zoomOutJulia() {

    float_type newDistRe = boundariesJulia.getDistRe() * zoomFactor;
    float_type newDistIm = boundariesJulia.getDistIm() * zoomFactor;
    zoomJulia *= zoomFactor;

    float_type offsetRe = (newDistRe - boundariesJulia.getDistRe()) / 2.0;
    float_type offsetIm = (newDistIm - boundariesJulia.getDistIm()) / 2.0;
    boundariesJulia.setMinRe(boundariesJulia.getMinRe() - offsetRe);
    boundariesJulia.setMinIm(boundariesJulia.getMinIm() - offsetIm);

    boundariesJulia.setMaxRe(boundariesJulia.getMaxRe() + offsetRe);
    boundariesJulia.setMaxIm(boundariesJulia.getMaxIm() + offsetIm);

    setBoundaryArgumentsJulia();
}

void FractalRenderer::moveJulia(int shiftX, int shiftY) {

    float_type shiftRe = onePixelSizeJulia.re * shiftX;
    float_type shiftIm = onePixelSizeJulia.im * shiftY;

    boundariesJulia.setMinRe(boundariesJulia.getMinRe() + shiftRe);
    boundariesJulia.setMinIm(boundariesJulia.getMinIm() + shiftIm);

    boundariesJulia.setMaxRe(boundariesJulia.getMaxRe() + shiftRe);
    boundariesJulia.setMaxIm(boundariesJulia.getMaxIm() + shiftIm);

    setBoundaryArgumentsJulia();
}

void FractalRenderer::windowSizeChanged(int width, int height) {
    this->width = width / 2;
    this->height = height;

    cout << "width = " << this->width << endl;
    cout << "height = " << this->height << endl;

    if (!inited)
        return;

    if (!initImageBuffer()) {
        cout << "ERROR: initPboBuffer() FAILED!!" << endl;
    } else {
        cout << "initPboBuffer() success :)" << endl;
    }
}

bool FractalRenderer::createProgramAndKernels() {

    cl_int err;

    string sourceCode;
    if (!RevUtil::readFile(KERNEL_FILE, sourceCode)) {
        return false;
    }

    cl::Program::Sources
    sources(1, std::make_pair(sourceCode.c_str(), sourceCode.length()));

    clProgram = cl::Program(clContext, sources, &err);
    CHECK_ERR(err == CL_SUCCESS);

    string flags = "-cl-fast-relaxed-math -Icl_files/";
#ifdef USE_DOUBLE
    flags += " -DUSE_DOUBLE";
#endif

    err = clProgram.build(clDevices, flags.c_str());

    if (err != CL_SUCCESS) {

        string str =
                clProgram.getBuildInfo<CL_PROGRAM_BUILD_LOG > (clDevices[0]);
        cout << "Program Error Info: " << str << endl;
        return false;
    }

    kernelDrawMandelbrotFractal = cl::Kernel(clProgram,
            "drawMandelbrotFractal", &err);
    CHECK_ERR(err == CL_SUCCESS);

    kernelDrawJuliaFractal = cl::Kernel(clProgram,
            "drawJuliaFractal", &err);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawJuliaFractal.setArg(
            DrawJuliaFractal::ARG_CONST_POINT, juliaConstPoint);
    CHECK_ERR(err == CL_SUCCESS);

    setIterations(iterations);
    setEscapeRadiusSqr(escapeRadiusSqr);
    setAALevel(aaLevel);

    return true;
}

size_t roundUp(int group_size, int global_size) {
    if (group_size == 0) return 0;
    int r = global_size % group_size;
    if (r == 0) {
        return global_size;
    } else {
        return global_size + group_size - r;
    }
}

bool FractalRenderer::initImageBuffer() {

    // calculate new grid size
    gridSize[0] = roundUp(LOCAL_SIZE_X, width);
    gridSize[1] = roundUp(LOCAL_SIZE_Y, height);

    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Mandelbrot:
    if (textureMandelbrot != 0) {
        glDeleteTextures(1, &textureMandelbrot);
        textureMandelbrot = 0;
    }

    //   glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &textureMandelbrot);
    glBindTexture(GL_TEXTURE_2D, textureMandelbrot);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);

    cl_int err;
    imgBufferMandelbrot = cl::Image2DGL(clContext(), CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, textureMandelbrot, &err);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawMandelbrotFractal.setArg(DrawMandelbrotFractal::ARG_OUTPUT, imgBufferMandelbrot);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawMandelbrotFractal.setArg(DrawMandelbrotFractal::ARG_IMAGE_W, width);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawMandelbrotFractal.setArg(DrawMandelbrotFractal::ARG_IMAGE_H, height);
    CHECK_ERR(err == CL_SUCCESS);
    //~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
    // Julia:
    if (textureJulia != 0) {
        glDeleteTextures(1, &textureJulia);
        textureJulia = 0;
    }

    //glActiveTexture(GL_TEXTURE0);
    glGenTextures(1, &textureJulia);
    glBindTexture(GL_TEXTURE_2D, textureJulia);

    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, width, height, 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);

    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);

    glBindTexture(GL_TEXTURE_2D, 0);

    imgBufferJulia = cl::Image2DGL(clContext(), CL_MEM_WRITE_ONLY, GL_TEXTURE_2D, 0, textureJulia, &err);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawJuliaFractal.setArg(DrawJuliaFractal::ARG_OUTPUT, imgBufferJulia);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawJuliaFractal.setArg(DrawJuliaFractal::ARG_IMAGE_W, width);
    CHECK_ERR(err == CL_SUCCESS);

    err = kernelDrawJuliaFractal.setArg(DrawJuliaFractal::ARG_IMAGE_H, height);
    CHECK_ERR(err == CL_SUCCESS);
    //==========================================================================

    setBoundaryArgumentsMandelbrot();
    setBoundaryArgumentsJulia();

    return true;
}

void FractalRenderer::setBoundaryArgumentsJulia() {
    onePixelSizeJulia.re = boundariesJulia.getDistRe() / (width - 1);
    onePixelSizeJulia.im = boundariesJulia.getDistIm() / (height - 1);

    setAAScale(antiAliasScale);

    cl_int err = kernelDrawJuliaFractal.setArg(
            DrawJuliaFractal::ARG_MIN_RE, boundariesJulia.getMinRe());
    assert(err == CL_SUCCESS);

    err = kernelDrawJuliaFractal.setArg(
            DrawJuliaFractal::ARG_MAX_IM, boundariesJulia.getMaxIm());
    assert(err == CL_SUCCESS);

    err = kernelDrawJuliaFractal.setArg(DrawJuliaFractal::ARG_ONE_PIXEL_SIZE,
            onePixelSizeJulia);
    assert(err == CL_SUCCESS);
}

void FractalRenderer::setBoundaryArgumentsMandelbrot() {
    onePixelSizeMandelbrot.re = boundariesMandelbrot.getDistRe() / (width - 1);
    onePixelSizeMandelbrot.im = boundariesMandelbrot.getDistIm() / (height - 1);

    setAAScale(antiAliasScale);

    cl_int err = kernelDrawMandelbrotFractal.setArg(
            DrawMandelbrotFractal::ARG_MIN_RE, boundariesMandelbrot.getMinRe());
    assert(err == CL_SUCCESS);

    err = kernelDrawMandelbrotFractal.setArg(
            DrawMandelbrotFractal::ARG_MAX_IM, boundariesMandelbrot.getMaxIm());
    assert(err == CL_SUCCESS);

    err = kernelDrawMandelbrotFractal.setArg(DrawMandelbrotFractal::ARG_ONE_PIXEL_SIZE,
            onePixelSizeMandelbrot);
    assert(err == CL_SUCCESS);
}



GLuint FractalRenderer::createQuadVAO(GLfloat xBegin, GLfloat xEnd) {
    // Array for full-screen quad
    GLfloat verts[] = {
        xBegin, -1.0f, 0.0f,
        xEnd, -1.0f, 0.0f,
        xEnd, 1.0f, 0.0f,
        xBegin, -1.0f, 0.0f,
        xEnd, 1.0f, 0.0f,
        xBegin, 1.0f, 0.0f
    };
    GLfloat tc[] = {
        0.0f, 0.0f,
        1.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 0.0f,
        1.0f, 1.0f,
        0.0f, 1.0f
    };

    // Set up the buffers

    unsigned int handle[2];
    glGenBuffers(2, handle);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 3 * sizeof (float), verts, GL_STATIC_DRAW);

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glBufferData(GL_ARRAY_BUFFER, 6 * 2 * sizeof (float), tc, GL_STATIC_DRAW);

    // Set up the vertex array object
    GLuint quadVAO = 0;
    glGenVertexArrays(1, &quadVAO);
    glBindVertexArray(quadVAO);

    glBindBuffer(GL_ARRAY_BUFFER, handle[0]);
    glVertexAttribPointer((GLuint) 0, 3, GL_FLOAT, GL_FALSE, 0, ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(0); // Vertex position

    glBindBuffer(GL_ARRAY_BUFFER, handle[1]);
    glVertexAttribPointer((GLuint) 1, 2, GL_FLOAT, GL_FALSE, 0, ((GLubyte *) NULL + (0)));
    glEnableVertexAttribArray(1); // Texture coordinates

    glBindVertexArray(0);

    return quadVAO;
}

