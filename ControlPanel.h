/* 
 * File:   ControlPanel.h
 * Author: Revers
 *
 * Created on 26 kwiecień 2012, 20:33
 */

#ifndef CONTROLPANEL_H
#define	CONTROLPANEL_H

#include <GL/glfw.h>
#include <AntTweakBar.h>

#include "Configuration.h"

class FractalRenderer;

class ControlPanel {

    struct TweakBarBounds {
        int x;
        int y;
        int width;
        int height;
    };

    TweakBarBounds settingsBarBounds;
    TwBar *settingsBar;

    FractalRenderer* fractalRenderer;
public:
    bool mouseOver;
 
    ControlPanel(FractalRenderer* fractalRenderer_);

    virtual ~ControlPanel() {
        TwTerminate();
    }

    bool init();

    void draw() {
        TwDraw();
    }

    void keyDownCallback(int key, int action) {
        TwEventKeyGLFW(key, action);
    }

    void resizeCallback(int width, int height) {
        TwWindowSize(width, height);
    }

    void mousePosCallback(int x, int y) {
        TwEventMousePosGLFW(x, y);
    }

    void mouseButtonCallback(int id, int state) {
        TwGetParam(settingsBar, NULL, "size", TW_PARAM_INT32, 2, &settingsBarBounds.width);
        TwGetParam(settingsBar, NULL, "position", TW_PARAM_INT32, 2, &settingsBarBounds.x);

        if (id == GLFW_MOUSE_BUTTON_LEFT) {
            if (state == GLFW_PRESS) {
                int x, y;
                glfwGetMousePos(&x, &y);

                if ((x >= settingsBarBounds.x
                        && x < settingsBarBounds.x + settingsBarBounds.width
                        && y >= settingsBarBounds.y
                        && y < settingsBarBounds.y + settingsBarBounds.height)
                        ) {
                    mouseOver = true;
                }
            } else {
                mouseOver = false;
            }
        }

        TwEventMouseButtonGLFW(id, state);
    }

    void mouseWheelCallback(int pos) {
        refresh();
        TwEventMouseWheelGLFW(pos);
    }
    
    void refresh() {
        TwRefreshBar(settingsBar);
    }

private:

    static void TW_CALL setEscapeRadiusSqrCallback(const void* value, void* clientData);

    static void TW_CALL getEscapeRadiusSqrCallback(void* value, void* clientData);

    static void TW_CALL setIterationsCallback(const void* value, void* clientData);

    static void TW_CALL getIterationsCallback(void* value, void* clientData);
    
    static void TW_CALL setAALevelCallback(const void* value, void* clientData);

    static void TW_CALL getAALevelCallback(void* value, void* clientData);
    
    static void TW_CALL setAAScaleCallback(const void* value, void* clientData);

    static void TW_CALL getAAScaleCallback(void* value, void* clientData);
    
    static void TW_CALL getZoomMandelbrotCallback(void* value, void* clientData);
    
    static void TW_CALL getMinReMandelbrotCallback(void* value, void* clientData);
    
    static void TW_CALL getMinImMandelbrotCallback(void* value, void* clientData);
    
    static void TW_CALL getMaxReMandelbrotCallback(void* value, void* clientData);
    
    static void TW_CALL getMaxImMandelbrotCallback(void* value, void* clientData);
    
    static void TW_CALL getZoomJuliaCallback(void* value, void* clientData);
    
    static void TW_CALL getMinReJuliaCallback(void* value, void* clientData);
    
    static void TW_CALL getMinImJuliaCallback(void* value, void* clientData);
    
    static void TW_CALL getMaxReJuliaCallback(void* value, void* clientData);
    
    static void TW_CALL getMaxImJuliaCallback(void* value, void* clientData);
    
    static void TW_CALL getJuliaPointReCallback(void* value, void* clientData);
    
    static void TW_CALL getJuliaPointImCallback(void* value, void* clientData);
};

#endif	/* CONTROLPANEL_H */

