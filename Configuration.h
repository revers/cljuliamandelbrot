/* 
 * File:   Configuration.h
 * Author: Revers
 *
 * Created on 26 kwiecień 2012, 20:57
 */

#ifndef CONFIGURATION_H
#define	CONFIGURATION_H

#define USE_DOUBLE

#ifdef USE_DOUBLE
#define float_type double
#else
#define float_type float
#endif

#define JULIA_ON_RIGHT_SIDE

#endif	/* CONFIGURATION_H */

