/* 
 * File:   MyCLFractals.cpp
 * Author: Revers
 *
 * Created on 17 marzec 2012, 15:54
 */

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#include <iostream>

#include <GL/glew.h>
#include <GL/glfw.h>
#include <GL/gl.h>

#include "ControlPanel.h"

#include "FPSCounter.h"
#include "FractalRenderer.h"

//#include "vboaxes.h"

//#include <SOIL.h>

using namespace std;

int running = 1;

//------------------------------------------------
// Defines:
//------------------------------------------------
#ifdef USE_DOUBLE
#define APP_TITLE "CLJuliaMandelbrot (Double precision)"
#else
#define APP_TITLE "CLJuliaMandelbrot (Single precision)"
#endif

#define FAILURE 1
#define SUCCESS 0

#define WINDOW_WIDTH 1280
#define WINDOW_HEIGHT 600

//------------------------------------------------
// Globals:
//------------------------------------------------
bool rmbPressed = false;
bool lmbPressed = false;
int lastMouseWheelPos = 0;
int lastX = 0;
int lastY = 0;

bool wireFrameMode = false;

FPSCounter fpsCounter;
FractalRenderer* fractalRenderer = NULL;
ControlPanel* controlPanel = NULL;

/* Draw view */
void renderScene() {
    /* Clear the color and depth buffers. */
    glClear(GL_COLOR_BUFFER_BIT);
    //glClear(GL_COLOR_BUFFER_BIT);

    if (wireFrameMode) {
        glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
    } else {
        glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
    }

    fractalRenderer->render();

    controlPanel->draw();

    glfwSwapBuffers();
}

/* Handle key strokes */
void GLFWCALL keyDownCallback(int key, int action) {
    if (action != GLFW_PRESS) {
        return;
    }

    switch (key) {
        case GLFW_KEY_ESC:
            running = 0;
            break;
        case GLFW_KEY_SPACE:
            wireFrameMode = !wireFrameMode;
            break;

        case 'a':
        case 'A':
        {

        }
            break;

        default:
            break;
    }
}

/* Callback function for window resize events */
void GLFWCALL resizeCallback(int width, int height) {
    controlPanel->resizeCallback(width, height);
    float ratio = 1.0f;

    if (height > 0) {
        ratio = (float) width / (float) height;
    }

    glViewport(0, 0, width, height);

    fractalRenderer->windowSizeChanged(width, height);
}

void GLFWCALL mousePosCallback(int x, int y) {

    controlPanel->mousePosCallback(x, y);

    if (rmbPressed) {

        int xDiff = lastX - x;
        int yDiff = lastY - y;

        controlPanel->refresh();
        if (fractalRenderer->isMandelbrotPart(x, y)) {
            fractalRenderer->moveMandelbrot(xDiff, yDiff);
        } else {
            fractalRenderer->moveJulia(xDiff, yDiff);
        }

        lastX = x;
        lastY = y;
    } else if (lmbPressed) {
        // controlPanel->refresh();
        fractalRenderer->chooseJuliaPoint(x, y);
        controlPanel->refresh();
    }
}

void GLFWCALL mouseButtonCallback(int id, int state) {

    controlPanel->mouseButtonCallback(id, state);

    if (id == GLFW_MOUSE_BUTTON_RIGHT) {
        if (state == GLFW_PRESS) {
            int x, y;
            glfwGetMousePos(&x, &y);

            lastX = x;
            lastY = y;
            if (!controlPanel->mouseOver) {
                rmbPressed = true;
            }
        } else {
            rmbPressed = false;
        }
    } else if (id == GLFW_MOUSE_BUTTON_LEFT) {
        if (state == GLFW_PRESS) {
            int x, y;
            glfwGetMousePos(&x, &y);

            if (!controlPanel->mouseOver) {
                lmbPressed = true;
                fractalRenderer->chooseJuliaPoint(x, y);
                controlPanel->refresh();
                
            }
        } else {
            lmbPressed = false;
        }
    }
}

void GLFWCALL mouseWheelCallback(int pos) {
    controlPanel->mouseWheelCallback(pos);

    int diff = pos - lastMouseWheelPos;
    lastMouseWheelPos = pos;

    if (!fractalRenderer->isInited()) {
        return;
    }

    int x, y;
    glfwGetMousePos(&x, &y);

    if (diff > 0) {
        if (fractalRenderer->isMandelbrotPart(x, y)) {
            fractalRenderer->zoomInMandelbrot();
        } else {
            fractalRenderer->zoomInJulia();
        }

    } else if (diff < 0) {
        if (fractalRenderer->isMandelbrotPart(x, y)) {
            fractalRenderer->zoomOutMandelbrot();
        } else {
            fractalRenderer->zoomOutJulia();
        }
    }
}

/* Initialize OpenGL */
int initializeGL() {

    glDisable(GL_DEPTH_TEST);

    glClearColor(1, 1, 1, 1);

    return SUCCESS;
}

/* Program entry point */
int main(int argc, char* argv[]) {
    srand(time(0));

    /* Dimensions of our window. */
    int width, height;
    /* Style of our window. */
    //int mode;

    /* Initialize GLFW */
    if (glfwInit() == GL_FALSE) {
        fprintf(stderr, "GLFW initialization failed\n");
        exit(-1);
    }

    /* Desired window properties */
    width = WINDOW_WIDTH;
    height = WINDOW_HEIGHT;
    //mode = GLFW_WINDOW;

    //glfwEnable(GLFW_AUTO_POLL_EVENTS); /* No explicit call to glfwPollEvents() */

    ///glfwOpenWindowHint(GLFW_WINDOW_NO_RESIZE, GL_TRUE);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MAJOR, 4);
    glfwOpenWindowHint(GLFW_OPENGL_VERSION_MINOR, 2);
    glfwOpenWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwOpenWindowHint(GLFW_OPENGL_FORWARD_COMPAT, GL_TRUE);


    GLFWvidmode mode; // GLFW video mo
    glfwGetDesktopMode(&mode);
    /* Open window */
    //if( !glfwOpenWindow(640, 480, mode.RedBits, mode.GreenBits, mode.BlueBits, 0, 16, 0, GLFW_WINDOW /* or GLFW_FULLSCREEN */) )
    if (glfwOpenWindow(width, height, mode.RedBits, mode.GreenBits, mode.BlueBits, 0, 16, 0, GLFW_WINDOW /* or GLFW_FULLSCREEN */) == GL_FALSE) {
        fprintf(stderr, "Could not open window\n");
        glfwTerminate();
        exit(-1);
    }

    glewInit();

    /* Set title */
    glfwSetWindowTitle(APP_TITLE);

    glfwSwapInterval(1);

    fractalRenderer = new FractalRenderer();
    controlPanel = new ControlPanel(fractalRenderer);
    
    if (controlPanel->init() == false) {
        cout << "ControlPanel::init() FAILED!";
        return -1;
    }

    /* Keyboard handler */
    glfwSetKeyCallback(keyDownCallback);
    glfwSetMousePosCallback(mousePosCallback);
    glfwSetMouseButtonCallback(mouseButtonCallback);
    glfwSetMouseWheelCallback(mouseWheelCallback);
    glfwSetWindowSizeCallback(resizeCallback);

    // - Directly redirect GLFW char events to AntTweakBar
    glfwSetCharCallback((GLFWcharfun) TwEventCharGLFW);

    glfwEnable(GLFW_KEY_REPEAT);

    /* Initialize OpenGL */
    if (initializeGL() == FAILURE) {
        cout << "ERROR: initializeGL FAILED!" << endl;
        return -1;
    }

    if (fractalRenderer->init() == false) {
        cout << "ERROR: fractalRnederer.init() FAILED!" << endl;
        return -1;
    }

    glfwSwapInterval(0);

    int glVersion[2] = {-1, -1};
    glGetIntegerv(GL_MAJOR_VERSION, &glVersion[0]);
    glGetIntegerv(GL_MINOR_VERSION, &glVersion[1]);

    std::cout << "Using OpenGL: " << glVersion[0] << "." << glVersion[1] << std::endl;

    /* Main loop */
    while (running) {

        fpsCounter.frameBegin();

        /* Draw wave grid to OpenGL display */
        renderScene();

        /* Still running? */
        running = running && glfwGetWindowParam(GLFW_OPENED);

        if (fpsCounter.frameEnd()) {
            char title[256];
            sprintf(title, APP_TITLE " | %d fps ", fpsCounter.getFramesPerSec());

            glfwSetWindowTitle(title);
        }
    }

    glfwTerminate();

    return 0;
}

