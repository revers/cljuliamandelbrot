/* 
 * File:   Boundaries.hpp
 * Author: Revers
 *
 * Created on 3 maj 2012, 09:28
 */

#ifndef BOUNDARIES_HPP
#define	BOUNDARIES_HPP

#include "Configuration.h"
#include <iostream>

class Boundaries {
private:
    float_type minRe;
    float_type maxRe;
    float_type minIm;
    float_type maxIm;
    float_type distRe;
    float_type distIm;

public:
    Boundaries() {}

    Boundaries(float_type minRe_, float_type maxRe_,
            float_type minIm_, float_type maxIm_) :
    minRe(minRe_),
    maxRe(maxRe_),
    minIm(minIm_),
    maxIm(maxIm_) {
        distRe = maxRe - minRe;
        distIm = maxIm - minIm;
    }

    float_type getMaxIm() const {
        return maxIm;
    }

    void setMaxIm(float_type maxIm) {
        this->maxIm = maxIm;

        distIm = maxIm - minIm;
    }

    float_type getMaxRe() const {
        return maxRe;
    }

    void setMaxRe(float_type maxRe) {
        this->maxRe = maxRe;

        distRe = maxRe - minRe;
    }

    float_type getMinIm() const {
        return minIm;
    }

    void setMinIm(float_type minIm) {
        this->minIm = minIm;

        distIm = maxIm - minIm;
    }

    float_type getMinRe() const {
        return minRe;
    }

    void setMinRe(float_type minRe) {
        this->minRe = minRe;

        distRe = maxRe - minRe;
    }

    float_type getDistIm() const {
        return distIm;
    }

    float_type getDistRe() const {
        return distRe;
    }

    friend std::ostream&
    operator<<(std::ostream& os, const Boundaries & b) {
        return os << "Boundaries(" << b.minRe << ", " << b.maxRe << ", "
                << b.minIm << ", " << b.maxIm << ")";
    }
};

#endif	/* BOUNDARIES_HPP */

