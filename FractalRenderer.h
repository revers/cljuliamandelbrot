/* 
 * File:   FractalRenderer.h
 * Author: Revers
 *
 * Created on 17 marzec 2012, 16:39
 */

#ifndef FRACTALRENDERER_H
#define	FRACTALRENDERER_H

#include "OpenCLUtil.h"
#include "OpenCLBase.h"

#include "glslprogram.h"
#include <stack>
#include <iostream>

#include "Configuration.h"
#include "ComplexType.hpp"

#include "Boundaries.hpp"
#include "DrawFractalArguments.hpp"

class FractalRenderer : public OpenCLBase {
    cl::Kernel kernelDrawMandelbrotFractal;
    cl::Image2DGL imgBufferMandelbrot;
    Boundaries boundariesMandelbrot;
    Complex onePixelSizeMandelbrot;
    float_type zoomMandelbrot;
    GLuint quadVAOMandelbrot;
    GLuint textureMandelbrot;

    cl::Kernel kernelDrawJuliaFractal;
    cl::Image2DGL imgBufferJulia;
    Boundaries boundariesJulia;
    Complex onePixelSizeJulia;
    float_type zoomJulia;
    GLuint quadVAOJulia;
    GLuint textureJulia;
    Complex juliaConstPoint;

    bool inited;
    int width;
    int height;

    cl::Program clProgram;
    cl::Image2D palletBuffer;
    size_t gridSize[2];
    GLSLProgram fractalProgram;

    float_type zoomFactor;
    unsigned int iterations;
    float_type escapeRadiusSqr;
    int aaLevel;
    float_type aaLevelInv;
    float_type antiAliasScale;
public:

    FractalRenderer() {
        inited = false;
        width = 0;
        height = 0;

        boundariesMandelbrot = Boundaries(-2.0, 1.0, -1.4, 1.4);
        zoomMandelbrot = 1.0;
        textureMandelbrot = 0;
        quadVAOMandelbrot = 0;

        boundariesJulia = boundariesMandelbrot;
        zoomJulia = 1.0;
        textureJulia = 0;
        quadVAOJulia = 0;
        juliaConstPoint = Complex(0.296257, 0.481603);
                //Complex(0.323164, 0.0447992);

        zoomFactor = 1.3;
        antiAliasScale = 1.2;
        aaLevel = 3;
        aaLevelInv = 1.0 / (float_type) aaLevel;
        escapeRadiusSqr = 100.0;
        iterations = 328;

        gridSize[0] = 0;
        gridSize[1] = 0;
    }

    ~FractalRenderer() {
        if (textureMandelbrot) {
            glDeleteTextures(1, &textureMandelbrot);
        }
    }

    void zoomInMandelbrot();

    void zoomOutMandelbrot();

    void moveMandelbrot(int shiftX, int shiftY);

    void zoomInJulia();

    void zoomOutJulia();

    bool isMandelbrotPart(int x, int y) {
#ifdef JULIA_ON_RIGHT_SIDE
        if (x >= width) {
            return false;
        }
        return true;
#else
        if (x < width) {
            return false;
        }
        return true;
#endif
    }

    void moveJulia(int shiftX, int shiftY);

    bool isInited() {
        return inited;
    }

    bool init();

    void render();

    void windowSizeChanged(int width, int height);

    float_type getEscapeRadiusSqr() const {
        return escapeRadiusSqr;
    }

    void setEscapeRadiusSqr(float_type escapeRadiusSqr) {
        this->escapeRadiusSqr = escapeRadiusSqr;

        cl_int err = kernelDrawMandelbrotFractal.setArg(
                DrawMandelbrotFractal::ARG_ESCAPE_RADIUS_SQR, escapeRadiusSqr);
        assert(err == CL_SUCCESS);

        err = kernelDrawJuliaFractal.setArg(
                DrawJuliaFractal::ARG_ESCAPE_RADIUS_SQR, escapeRadiusSqr);
        assert(err == CL_SUCCESS);
    }

    unsigned int getIterations() const {
        return iterations;
    }

    void setIterations(unsigned int iterations) {
        this->iterations = iterations;

        cl_int err = kernelDrawMandelbrotFractal.setArg(
                DrawMandelbrotFractal::ARG_ITERATIONS, iterations);
        assert(err == CL_SUCCESS);

        err = kernelDrawJuliaFractal.setArg(
                DrawJuliaFractal::ARG_ITERATIONS, iterations);
        assert(err == CL_SUCCESS);
    }

    void chooseJuliaPoint(int mouseX, int mouseY) {
        if (!isMandelbrotPart(mouseX, mouseY)) {
            return;
        }

#ifndef JULIA_ON_RIGHT_SIDE
        mouseX -= width;
#endif

        Complex constPoint(
                boundariesMandelbrot.getMinRe() + onePixelSizeMandelbrot.re * (float_type) mouseX,
                boundariesMandelbrot.getMinIm() + onePixelSizeMandelbrot.im * (float_type) mouseY);

        setJuliaConstPoint(constPoint);
    }

    Complex getJuliaConstPoint() const {
        return juliaConstPoint;
    }

    void setJuliaConstPoint(Complex constPoint) {
        juliaConstPoint = constPoint;
        
        cl_int err = kernelDrawMandelbrotFractal.setArg(
                DrawMandelbrotFractal::ARG_CONST_POINT, juliaConstPoint);
        assert(err == CL_SUCCESS);

        err = kernelDrawJuliaFractal.setArg(
                DrawJuliaFractal::ARG_CONST_POINT, juliaConstPoint);
        assert(err == CL_SUCCESS);
    }

    int getAALevel() const {
        return aaLevel;
    }

    void setAALevel(int aaLevel) {
        this->aaLevel = aaLevel;

        aaLevelInv = 1.0 / (float_type) aaLevel;

        cl_int err = kernelDrawMandelbrotFractal.setArg(
                DrawMandelbrotFractal::ARG_AA_LEVEL, aaLevel);
        assert(err == CL_SUCCESS);

        err = kernelDrawMandelbrotFractal.setArg(
                DrawMandelbrotFractal::ARG_AA_LEVEL_INV, aaLevelInv);
        assert(err == CL_SUCCESS);

        err = kernelDrawJuliaFractal.setArg(
                DrawJuliaFractal::ARG_AA_LEVEL, aaLevel);
        assert(err == CL_SUCCESS);

        err = kernelDrawJuliaFractal.setArg(
                DrawJuliaFractal::ARG_AA_LEVEL_INV, aaLevelInv);
        assert(err == CL_SUCCESS);
    }

    float getAAScale() const {
        return antiAliasScale;
    }

    void setAAScale(float antiAliasScale) {
        this->antiAliasScale = antiAliasScale;

        Complex aaScaleMandelbrot(onePixelSizeMandelbrot.re * antiAliasScale,
                onePixelSizeMandelbrot.im * antiAliasScale);

        cl_int err = kernelDrawMandelbrotFractal.setArg(
                DrawMandelbrotFractal::ARG_AA_SCALE, aaScaleMandelbrot);
        assert(err == CL_SUCCESS);

        Complex aaScaleJulia(onePixelSizeJulia.re * antiAliasScale,
                onePixelSizeJulia.im * antiAliasScale);

        err = kernelDrawJuliaFractal.setArg(
                DrawJuliaFractal::ARG_AA_SCALE, aaScaleJulia);
        assert(err == CL_SUCCESS);
    }

    float_type getZoomMandelbrot() const {
        return zoomMandelbrot;
    }

    float_type getMinReMandelbrot() const {
        return boundariesMandelbrot.getMinRe();
    }

    float_type getMinImMandelbrot() const {
        return boundariesMandelbrot.getMinIm();
    }

    float_type getMaxReMandelbrot() const {
        return boundariesMandelbrot.getMaxRe();
    }

    float_type getMaxImMandelbrot() const {
        return boundariesMandelbrot.getMaxIm();
    }

    float_type getZoomJulia() const {
        return zoomJulia;
    }

    float_type getMinReJulia() const {
        return boundariesJulia.getMinRe();
    }

    float_type getMinImJulia() const {
        return boundariesJulia.getMinIm();
    }

    float_type getMaxReJulia() const {
        return boundariesJulia.getMaxRe();
    }

    float_type getMaxImJulia() const {
        return boundariesJulia.getMaxIm();
    }

private:
    bool createProgramAndKernels();

    bool initImageBuffer();

    void drawFractal();

    GLuint createQuadVAO(GLfloat xBegin, GLfloat xEnd);

    void setBoundaryArgumentsMandelbrot();

    void setBoundaryArgumentsJulia();

};

#endif	/* FRACTALRENDERER_H */

