/* 
 * File:   ControlPanel.cpp
 * Author: Revers
 * 
 * Created on 26 kwiecień 2012, 20:33
 */
#include <GL/glew.h>

#include "ControlPanel.h"

#include <cmath>
#include <iostream>

#include "FractalRenderer.h"

using namespace std;

#define SETTINGS_TOOLBAR_WIDTH "290"
#define SETTINGS_TOOLBAR_HEIGHT "360"

ControlPanel::ControlPanel(FractalRenderer* fractalRenderer_) :
fractalRenderer(fractalRenderer_) {

    settingsBar = NULL;
    mouseOver = false;
}

bool ControlPanel::init() {
    if (!TwInit(TW_OPENGL_CORE, NULL)) {
        cout << TwGetLastError() << endl;
        return false;
    }

    settingsBar = TwNewBar("Settings");

    //   TwDefine(" GLOBAL help='This example shows how to integrate AntTweakBar with GLUT and OpenGL.' "); 
    TwDefine(" TW_HELP visible=false ");
    TwDefine(" Settings size='" SETTINGS_TOOLBAR_WIDTH " " SETTINGS_TOOLBAR_HEIGHT "' color='80 80 80' ");
    TwDefine(" Settings valueswidth=100 ");

    TwAddVarCB(settingsBar, "Iterations", TW_TYPE_INT32, setIterationsCallback, getIterationsCallback, this,
            " min=20 max=2000 step=10 group='Main' ");

#ifdef USE_DOUBLE    
#define REV_TW_TYPE_FLOAT TW_TYPE_DOUBLE
#else
#define REV_TW_TYPE_FLOAT TW_TYPE_FLOAT
#endif

    TwAddVarCB(settingsBar, "Escape Radius Square", REV_TW_TYPE_FLOAT, setEscapeRadiusSqrCallback, getEscapeRadiusSqrCallback, this,
            " min=4.0 max=10000.0 step=10.0 group='Main' ");

    TwAddVarCB(settingsBar, "Level", TW_TYPE_INT32, setAALevelCallback, getAALevelCallback, this,
            " min=1 max=7 step=1 group='Antialiasing' ");

    TwAddVarCB(settingsBar, "Scale", TW_TYPE_FLOAT, setAAScaleCallback, getAAScaleCallback, this,
            " min=1.0 max=25.0 step=0.1 group='Antialiasing' ");


    TwAddVarCB(settingsBar, "Zoom Factor", REV_TW_TYPE_FLOAT, NULL, getZoomMandelbrotCallback, this,
            " group='Info Mandelbrot' ");

    TwAddVarCB(settingsBar, "Minimum Real", REV_TW_TYPE_FLOAT, NULL, getMinReMandelbrotCallback, this,
            " group='Info Mandelbrot' ");

    TwAddVarCB(settingsBar, "Minimum Imaginary", REV_TW_TYPE_FLOAT, NULL, getMinImMandelbrotCallback, this,
            " group='Info Mandelbrot' ");

    TwAddVarCB(settingsBar, "Maximum Real", REV_TW_TYPE_FLOAT, NULL, getMaxReMandelbrotCallback, this,
            " group='Info Mandelbrot' ");

    TwAddVarCB(settingsBar, "Maximum Imaginary", REV_TW_TYPE_FLOAT, NULL, getMaxImMandelbrotCallback, this,
            " group='Info Mandelbrot' ");

    TwAddVarCB(settingsBar, "Zoom Factor Julia", REV_TW_TYPE_FLOAT, NULL, getZoomJuliaCallback, this,
            " group='Info Julia' label='Zoom Factor' ");

    TwAddVarCB(settingsBar, "Minimum Real Julia", REV_TW_TYPE_FLOAT, NULL, getMinReJuliaCallback, this,
            " group='Info Julia' label='Minimum Real' ");

    TwAddVarCB(settingsBar, "Minimum Imaginary Julia", REV_TW_TYPE_FLOAT, NULL, getMinImJuliaCallback, this,
            " group='Info Julia' label='Minimum Imaginary' ");

    TwAddVarCB(settingsBar, "Maximum Real Julia", REV_TW_TYPE_FLOAT, NULL, getMaxReJuliaCallback, this,
            " group='Info Julia' label='Maximum Real' ");

    TwAddVarCB(settingsBar, "Maximum Imaginary Julia", REV_TW_TYPE_FLOAT, NULL, getMaxImJuliaCallback, this,
            " group='Info Julia' label='Maximum Imagirnary' ");

    TwAddVarCB(settingsBar, "Point Real", REV_TW_TYPE_FLOAT, NULL, getJuliaPointReCallback, this,
            " group='Info Julia' ");

    TwAddVarCB(settingsBar, "Point Imaginary", REV_TW_TYPE_FLOAT, NULL, getJuliaPointImCallback, this,
            " group='Info Julia' ");

    return true;
}

void TW_CALL ControlPanel::setEscapeRadiusSqrCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fractalRenderer->setEscapeRadiusSqr(*(const float_type*) value);
}

void TW_CALL ControlPanel::getEscapeRadiusSqrCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getEscapeRadiusSqr();
}

void TW_CALL ControlPanel::setIterationsCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fractalRenderer->setIterations(*(const unsigned int*) value);
}

void TW_CALL ControlPanel::getIterationsCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(unsigned int*) value = controlPanel->fractalRenderer->getIterations();
}

void TW_CALL ControlPanel::setAALevelCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fractalRenderer->setAALevel(*(const int*) value);
}

void TW_CALL ControlPanel::getAALevelCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(int*) value = controlPanel->fractalRenderer->getAALevel();
}

void TW_CALL ControlPanel::setAAScaleCallback(const void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    controlPanel->fractalRenderer->setAAScale(*(const float*) value);
}

void TW_CALL ControlPanel::getAAScaleCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float*) value = controlPanel->fractalRenderer->getAAScale();
}

void TW_CALL ControlPanel::getZoomMandelbrotCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getZoomMandelbrot();
}

void TW_CALL ControlPanel::getMinReMandelbrotCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMinReMandelbrot();
}

void TW_CALL ControlPanel::getMinImMandelbrotCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMinImMandelbrot();
}

void TW_CALL ControlPanel::getMaxReMandelbrotCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMaxReMandelbrot();
}

void TW_CALL ControlPanel::getMaxImMandelbrotCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMaxImMandelbrot();
}

void TW_CALL ControlPanel::getZoomJuliaCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getZoomJulia();
}

void TW_CALL ControlPanel::getMinReJuliaCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMinReJulia();
}

void TW_CALL ControlPanel::getMinImJuliaCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMinImJulia();
}

void TW_CALL ControlPanel::getMaxReJuliaCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMaxReJulia();
}

void TW_CALL ControlPanel::getMaxImJuliaCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getMaxImJulia();
}

void TW_CALL ControlPanel::getJuliaPointReCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getJuliaConstPoint().re;
}

void TW_CALL ControlPanel::getJuliaPointImCallback(void* value, void* clientData) {
    ControlPanel* controlPanel = static_cast<ControlPanel*> (clientData);

    *(float_type*) value = controlPanel->fractalRenderer->getJuliaConstPoint().im;
}