#include "../fake_include/glsl_fake.hxx"

//$END_FAKE$

//-- Vertex
#version 400

layout (location = 0) in vec3 VertexPosition;
layout (location = 1) in vec2 VertexTexCoord;

out vec2 TexCoord;

void main() {
    TexCoord = VertexTexCoord;
    gl_Position = vec4(VertexPosition,1.0);
}

//-- Fragment
#version 400

in vec2 TexCoord;

uniform sampler2D Tex1;

layout(location = 0) out vec4 FragColor;

void main() {
    FragColor = texture(Tex1, TexCoord);
}
